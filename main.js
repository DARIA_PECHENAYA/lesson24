// const test = new Set();
// const obj = {name:'test'};
// test.addEventListener({name:'Daria'});
// test.addEventListener({name:'test'});
// test.addEventListener({name:'test'});
// console.log(test);

const testMap = new Map();
const keyobj = {key:"value"};
testMap.set('text', 123);
testMap.set({key:"value"},false);
testMap.set({key:"value"}, true);
for(let item of testMap) {
    console.log(item)
}
// console.log(testMapget(keyObj));

function first() {
    this.name = "testName";
    this.lastName = "lastName";
    second.call(this);
}
function second(){
    console.log(this.name);
    console.log(this.lastName);
}
first();

function first() {
    this.name = "testName";
    this.lastName = "lastName";
    second.call(this,'test@emaol.com','newArg');
}
function second(email){
    console.log(this.name);
    console.log(this.lastName);
    console.log(email);
    console.log(arguments);
}
first();

function first() {
    this.name = "testName";
    this.lastName = "lastName";
    second.apply(this,['test@emaol.com','newArg']);
}
function second(email){
    console.log(this.name);
    console.log(this.lastName);
    console.log(email);
    console.log(arguments);
}
first();

function sum(number1,number2) {
    let count = number1 + number2;
    console.log(count);
}
sum(1,2);
sum(1,2);

// Array.prototype.forEach = function() {};

/*ответ с задержкой в 10 сек*/

// let waitCall = new Promise((resolve, reject) =>{
//     setTimeout(() => {
//         resolve();
//     },10000);
// });

// waitCall.then(() =>{
//     console.log('Hello')
// });

// let waitCall = new Promise((resolve, reject) =>{
//     setTimeout(() => {
//         reject('text Error');
//     },100);
// });

// waitCall.then(() =>{
// }).catch((error) => {
//     console.log(error);
// });

/*не палит*/

// reject("text Error");
// waitCall.then(() => {

// }).then.then().then.catch().then();

/*async - await*/

async function doThem () {
    let waitPromise = new Promise ((resolve) => {
        setTimeout(() => {
            resolve('text');
        },5000)
    });
    let result = await waitPromise;
    console.log(result);
}
doThem ();
  
/*в случае ошибки в try, обрабатывается код в catch*/

try {
    let user;
    console.log(user.name);
} catch(e) {
    console.log('у вас ошибка');
}